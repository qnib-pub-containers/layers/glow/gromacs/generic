# GROMACS

To run a small example you'll need to download (and unzip) the input file and run `gmx mdrun`

## Prepare

```bash
wget https://www.mpinat.mpg.de/benchMEM
unzip benchMEM
rm -f benchMEM
```
## Run

### Docker

```bash
docker run -ti -v $(pwd):/data -w /data \
  metahub-registry.org/gromacs/tmpi:2021.5 \
  gmx mdrun -s benchMEM.tpr -resethway
```



