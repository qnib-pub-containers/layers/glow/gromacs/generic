# syntax = docker/dockerfile:1.4
FROM registry.gitlab.com/qnib-pub-containers/layers/glow/base:2023-06-12 AS glow

FROM scratch
ENV HPC3_HELP=/usr/share/hpc3/help.md
COPY --from=glow /opt/glow/glow /usr/bin/glow
COPY hpc3-help /usr/bin/hpc3-help
COPY help.md /usr/share/hpc3/help.md
